<?php
use Migrations\AbstractMigration;

class FixingTags extends AbstractMigration
{

    public function up()
    {

        $this->table('tags_tagged')
            ->changeColumn('fk_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('tags_tagged')
            ->changeColumn('fk_id', 'uuid', [
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }
}

